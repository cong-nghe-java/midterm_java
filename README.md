# Midterm_JAVA



## Getting started

### 1. Description 
### 2. ERD
### 3. Screenshort API
### 4. Implementation instructions
### 5. Demo


## 1. Project descritionn 

The project is a website selling jewellry.
 - Hierarchical implementation: administrators and users. The project
   used spring security 6.
 - Project functions, by role:
	 - **Administrators**: Display products, add, delete, and basic edits.
	 - **User**:
	 	- Login, register
		 - Display products by category, sort by price, sort by name, search products by name, search for products within price range
		 - View product details
		 - Add products to the cart, add, delete, and edit products in the cart.
		 - Order.

## 2.ERD
- This is the ERD image of the project
![ERD](./erd.png)

## 3.API

### Product
 - Get all product
![Get all product](./ScreenshortAPI/GetAllProduct.png)

 - Add product
![Add product](./ScreenshortAPI/AddProduct.png)

 - Update product
 ![Update product](./ScreenshortAPI/UpdateProduct.png)

 - Delete product
 ![Delete product](./ScreenshortAPI/DeleteProduct.png)

 ### Order
 - Get order of user
![Get order](./ScreenshortAPI/GetOrder.png)

 - Add product to shopping cart
![Add product to shopping cart](./ScreenshortAPI/AddProductToShoppingCart.png)

 - Update order
 ![Update order](./ScreenshortAPI/UpdateProductQuantityInShoppingCart.png)

 - Delete order
 ![Delete order](./ScreenshortAPI/DeleteProductInShoppingCart.png)

## 4.Implementation instructions
- Step 1: Clone the source code using the command:
```bash
git clone https://gitlab.com/cong-nghe-java/midterm_java
```
- Step 2: Turn on xampp, access phpmyadmin, and import the database file in the folder you just cloned.
- Step 3: Open the project with Intellij and access the file src/main/resources/application.properties, change the username and password of xampp (If you have ever changed the username and password of xampp)
```bash
spring.datasource.username=root
spring.datasource.password=
```
- Step 4: Running projects

## 5. Demo
```bash
https://www.youtube.com/watch?v=FSF0YafRNUc
```
